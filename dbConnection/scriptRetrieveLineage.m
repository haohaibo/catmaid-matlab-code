%name of the CATMAID project to retrieve tracking information
projectName = '13-03-12-Platenaries-CLAHE TGMM noSL'

%retrieve information to connect to the database
[username, password, databasename, databaseURL] = readDatabaseInfo();


%establish connection with database
connDB = connectToDatabase(databasename, databaseURL, username, password);

%retrieve information from the project
[ projectId, stackId, stackSize, stackRes ] = retrieveProjectInformation( connDB, projectName);


%retrieve all the tracked points from the database and store them into
%trackingM Nx10 array
%we follow the SWC convention (more or less, since we need time )
%id, type, x, y, z, radius, parent_id, time, confidence, skeleton_id
[trackingM,tags] = retrieveLineage( connDB, projectId );