function connDB = connectToDatabase(databasename, databaseURL, username, password)

% Add class path (if not in the class path)
pathstr = fileparts( mfilename('fullpath') );
p = [pathstr '/postgresql-9.2-1002.jdbc4.jar'];
if ~ismember(p, javaclasspath)
    javaaddpath(p)
end;

% connDB = database(databasename, username, password, 'Vendor', 'PostGreSQL');
connDB = database(databasename, username, password, 'org.postgresql.Driver', databaseURL);