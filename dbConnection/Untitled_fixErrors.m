%change errors in DB when parent(id).t > id.t

projectName = '12-08-28 Drosophila TGMM TM1000 Neuroblast'; %project containing the ground truth in CATMAID

%----------------------------------------------------------------------
%obtain annotated ground truth from CATMAID database for a specific project
%retrieve information to connect to the database
[username, password, databasename, databaseURL] = readDatabaseInfo();


setdbprefs('DataReturnFormat','numeric');%location_t cannot be retrieved as numeric
%connect to database
connDB = connectToDatabase(databasename, databaseURL, username, password);
[ projectId, stackId, stackSize, stackRes ] = retrieveProjectInformation( connDB, projectName);


%%
%reverse parents that fo from t to t+1
%treenodeIdVec = [5660429 5657393 15923547 7072474];
treenodeIdVec = [7600360];
nn = 0;
sqlCmd = cell(1,1);
for kk = 1: length(treenodeIdVec)

    trueParent = -1;
    currentNode = treenodeIdVec(kk);
    while(1)
        curs = exec(connDB,['SELECT parent_id,location_t FROM treenode WHERE id=' num2str(currentNode)  ' ORDER BY location_t']);
        curs = fetch( curs );
        
        currentNode_T = curs.Data{1,2};
        nextNode = curs.Data{1,1};
        
        curs = exec(connDB,['SELECT location_t FROM treenode WHERE id=' num2str(nextNode)  ' ORDER BY location_t']);
        curs = fetch( curs );
        if( isempty(curs.Data(1,1)) )
            break;
        end
        nextNode_T = curs.Data{1,1};
        
        if( nextNode_T < currentNode_T )
            break;%expected order
        end
        
        %change order        
        nn = nn +1;
        sqlCmd{nn} = ['UPDATE treenode SET parent_id= ' num2str(trueParent) ' WHERE id=' num2str(currentNode)];
        
        %update id for next iteration
        trueParent = currentNode;
        currentNode =  nextNode;
    end
end

close(curs);
close(connDB);