function [thumbnail, hSV, thumbnail_xz, hSV_xz, thumbnail_yz, hSV_yz] = generateThumbnail(xyz, stack, winSize, anisotropy, svPixelIdxList)

slab = 1;%number of Z planes to calculate MIP

%before hand to avoid error if early return
thumbnail = zeros(winSize * 2 + 1,'uint16');
thumbnail_xz = zeros(winSize * 2 + 1,'uint16');
thumbnail_yz = zeros(winSize * 2 + 1,'uint16');
hSV = [];
hSV_xz = [];
hSV_yz = [];


for ii = 1:2
    if( xyz(ii) - winSize < 1 || xyz(ii) + winSize > size(stack,ii) )
        thumbnail = zeros(winSize * 2 + 1,'uint16');
        return
    end
end
if( xyz(3) - slab < 1 || xyz(3) + slab > size(stack,3) )
    thumbnail = zeros(winSize * 2 + 1,'uint16');
    return
end

thumbnail = stack( round(xyz(1) - winSize): round(xyz(1) + winSize),round(xyz(2) - winSize): round(xyz(2) + winSize), round(xyz(3) - slab): round(xyz(3) + slab));

%normalize
thumbnail = single(thumbnail);
thr = prctile(thumbnail(:),[1 99]);
thumbnail = (thumbnail - thr(1))/(thr(2)-thr(1));
thumbnail( thumbnail > 1.0 ) = 1.0;
thumbnail( thumbnail < 0.0 ) = 0.0;
thumbnail = uint16( 65535 * thumbnail );
thumbnail( thumbnail < 1 ) = 1;%so I can add information on centroids setting pixels to 0

%MIP
thumbnail = max(thumbnail,[],3);

if( nargout > 1 ) %generate invisible figure with superovxle contour superimposed
    hSV = figure('visible','off');
    imagesc(thumbnail);colormap gray;axis off;
    
    imSize = size(stack);
    [xsv, ysv,zsv] = ind2sub(imSize,svPixelIdxList + 1);
    
    xsv = xsv - round(xyz(1) - winSize) + 1;
    ysv = ysv - round(xyz(2) - winSize) + 1;
    zsv = zsv - round(xyz(3) - slab) + 1;
    
    %remove any component outside the slab
    pp = find( xsv < 1 | ysv < 1 | zsv < 1 | xsv > ( 2 * winSize + 1) | ysv > ( 2 * winSize + 1) | zsv > ( 2 * slab + 1) );
    
    if( length(xsv) ~= length(pp) )%supervoxel does not intersect with plane
        xsv( pp ) =[];
        ysv( pp ) =[];
        zsv( pp ) =[];
        
        %draw contour
        bw = false( 2 * winSize + 1 );
        ind = sub2ind(size(bw), xsv, ysv);
        bw(ind) = true;
        [val, pos] = min(ind);%to start tracing boundary
        
        try %to avoid that the whole program breaks and stops
            contour = bwtraceboundary(bw, [xsv(pos) ysv(pos)],'E',8);
        catch exception
            
            try
                contour = bwtraceboundary(bw, [xsv(pos) ysv(pos)],'W',8);
            catch exception2
                warning 'Bwtrace boundary could not be executed'
                contour = [1 1];%just a green dot on the corner
            end
        end
        
        hold on;
        plot(get(hSV,'CurrentAxes'),contour(:,2),contour(:,1),'g','LineWidth',2);
        hold off;
    end
end

%====================================================================================
if( nargout > 2 ) %plane XZ
    
    winSize_z = round( winSize / anisotropy );
    
    
    if( xyz(1) - winSize < 1 || xyz(1) + winSize > size(stack,1) )
        thumbnail_xz = zeros(winSize * 2 + 1,'uint16');
        return
    end
    
    if( xyz(2) - slab < 1 || xyz(2) + slab > size(stack,2) )
        thumbnail_xz = zeros(winSize * 2 + 1,'uint16');
        return
    end
    
    if( xyz(3) - winSize_z < 1 || xyz(3) + winSize_z > size(stack,3) )
        thumbnail_xz = zeros(winSize * 2 + 1,'uint16');
        return
    end
    
    thumbnail_xz = stack( round(xyz(1) - winSize): round(xyz(1) + winSize),round(xyz(2) - slab): round(xyz(2) + slab), round(xyz(3) - winSize_z): round(xyz(3) + winSize_z));
    
    thumbnail_xz = permute( thumbnail_xz, [1 3 2] );
    
    %normalize
    thumbnail_xz = single(thumbnail_xz);
    thr = prctile(thumbnail_xz(:),[1 99]);
    thumbnail_xz = (thumbnail_xz - thr(1))/(thr(2)-thr(1));
    thumbnail_xz( thumbnail_xz > 1.0 ) = 1.0;
    thumbnail_xz( thumbnail_xz < 0.0 ) = 0.0;
    thumbnail_xz = uint16( 65535 * thumbnail_xz );
    thumbnail_xz( thumbnail_xz < 1 ) = 1;%so I can add information on centroids setting pixels to 0
    
    %MIP
    thumbnail_xz = max(thumbnail_xz,[],3);
    
    if( nargout > 3 ) %generate invisible figure with superovxle contour superimposed
        hSV_xz = figure('visible','off');
        imagesc(thumbnail_xz);colormap gray;axis off;
        
        imSize = size(stack);
        [xsv, ysv,zsv] = ind2sub(imSize,svPixelIdxList + 1);
        
        xsv = xsv - round(xyz(1) - winSize) + 1;
        ysv = ysv - round(xyz(2) - slab) + 1;
        zsv = zsv - round(xyz(3) - winSize_z) + 1;
        
        %remove any component outside the slab
        pp = find( xsv < 1 | ysv < 1 | zsv < 1 | xsv > ( 2 * winSize + 1) | ysv > ( 2 * slab + 1) | zsv > ( 2 * winSize_z + 1) );
        
        if( length(xsv) ~= length(pp) )%supervoxel does not intersect with plane
            xsv( pp ) =[];
            ysv( pp ) =[];
            zsv( pp ) =[];
            
            %draw contour
            bw = false( 2 * winSize + 1 , 2 * winSize_z + 1 );
            ind = sub2ind(size(bw), xsv, zsv);
            bw(ind) = true;
            [val, pos] = min(ind);%to start tracing boundary
            
            try %to avoid that the whole program breaks and stops
                contour = bwtraceboundary(bw, [xsv(pos) zsv(pos)],'E',8);
            catch exception
                
                try
                    contour = bwtraceboundary(bw, [xsv(pos) zsv(pos)],'W',8);
                catch exception2
                    warning 'Bwtrace boundary could not be executed'
                    contour = [1 1];%just a green dot on the corner
                end
            end
            
            hold on;
            plot(get(hSV_xz,'CurrentAxes'),contour(:,2),contour(:,1),'g','LineWidth',2);
            hold off;
        end
    end
end


%====================================================================================
if( nargout > 4 ) %plane YZ
    
    winSize_z = round( winSize / anisotropy );           
    
    if( xyz(1) - slab < 1 || xyz(1) + slab > size(stack,1) )
        thumbnail_yz = zeros(winSize * 2 + 1,'uint16');
        return
    end
    
    if( xyz(2) - winSize < 1 || xyz(2) + winSize > size(stack,2) )
        thumbnail_xz = zeros(winSize * 2 + 1,'uint16');
        return
    end
    
    if( xyz(3) - winSize_z < 1 || xyz(3) + winSize_z > size(stack,3) )
        thumbnail_xz = zeros(winSize * 2 + 1,'uint16');
        return
    end
    
    thumbnail_yz = stack( round(xyz(1) - slab): round(xyz(1) + slab),round(xyz(2) - winSize): round(xyz(2) + winSize), round(xyz(3) - winSize_z): round(xyz(3) + winSize_z));
    
    thumbnail_yz = permute( thumbnail_yz, [2 3 1] );
    
    %normalize
    thumbnail_yz = single(thumbnail_yz);
    thr = prctile(thumbnail_yz(:),[1 99]);
    thumbnail_yz = (thumbnail_yz - thr(1))/(thr(2)-thr(1));
    thumbnail_yz( thumbnail_yz > 1.0 ) = 1.0;
    thumbnail_yz( thumbnail_yz < 0.0 ) = 0.0;
    thumbnail_yz = uint16( 65535 * thumbnail_yz );
    thumbnail_yz( thumbnail_yz < 1 ) = 1;%so I can add information on centroids setting pixels to 0
    
    %MIP
    thumbnail_yz = max(thumbnail_yz,[],3);
    
    if( nargout > 5 ) %generate invisible figure with superovxle contour superimposed
        hSV_yz = figure('visible','off');
        imagesc(thumbnail_yz);colormap gray;axis off;
        
        imSize = size(stack);
        [xsv, ysv,zsv] = ind2sub(imSize,svPixelIdxList + 1);
        
        xsv = xsv - round(xyz(1) - slab) + 1;
        ysv = ysv - round(xyz(2) - winSize) + 1;
        zsv = zsv - round(xyz(3) - winSize_z) + 1;
        
        %remove any component outside the slab
        pp = find( xsv < 1 | ysv < 1 | zsv < 1 | xsv > ( 2 * slab + 1) | ysv > ( 2 * winSize + 1) | zsv > ( 2 * winSize_z + 1) );
        
        if( length(xsv) ~= length(pp) )%supervoxel does not intersect with plane
            xsv( pp ) =[];
            ysv( pp ) =[];
            zsv( pp ) =[];
            
            %draw contour
            bw = false( 2 * winSize + 1 , 2 * winSize_z + 1 );
            ind = sub2ind(size(bw), ysv, zsv);
            bw(ind) = true;
            [val, pos] = min(ind);%to start tracing boundary
            
            try %to avoid that the whole program breaks and stops
                contour = bwtraceboundary(bw, [ysv(pos) zsv(pos)],'E',8);
            catch exception
                
                try
                    contour = bwtraceboundary(bw, [ysv(pos) zsv(pos)],'W',8);
                catch exception2
                    warning 'Bwtrace boundary could not be executed'
                    contour = [1 1];%just a green dot on the corner
                end
            end
            
            hold on;
            plot(get(hSV_yz,'CurrentAxes'),contour(:,2),contour(:,1),'g','LineWidth',2);
            hold off;
        end
    end
end

