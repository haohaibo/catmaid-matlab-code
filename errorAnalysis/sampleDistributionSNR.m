%return the SNR for a subset of points in an image
function SNRcontrast = sampleDistributionSNR(xyz,im)

wInner = [15 15 5];%radius of window around a point: it has to be larger than the size of the largest object

SNRcontrast = zeros(size(xyz,1),1);

ww = zeros(1,6);
for ii = 1:length(SNRcontrast)
        
    %get a patch aroun point to get min/max intensity
    pp = round(xyz(ii,:));
    for jj = 1:3
       max_ = min(pp(jj)+wInner(jj),size(im,jj));
       min_ = max(pp(jj)-wInner(jj),1);
       
       ww(jj) = min_;
       ww(jj+3) = max_;
    end
    
    patch = single(im(ww(1):ww(4), ww(2):ww(5), ww(3):ww(6)));
    aux = prctile(patch(:),[5 95]);
        
    Imax = aux(2);
    Imin = max(aux(1),1);
    SNRcontrast(ii) = (Imax - Imin)/Imin;
    
end

