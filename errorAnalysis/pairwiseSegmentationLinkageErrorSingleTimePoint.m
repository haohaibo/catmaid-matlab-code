%INPUT:

%imgPatternFilename: optional if we want to generate thumbnails to explore
%the mistakes and correct

%trackingMatrixGT: Nx10 array with GT from database
%trackingMatrixErrorCode: Nx1 array. 2->segm. & link. correct; 1->segm.
%correct; 0->incorret

%we just look at solution at TM and TM+1 (basically pairwise random annotations)
function [timeT,numSegmentationTotal, numSegmentationCorrect, numLinkageTotal, numLinkageCorrect,trackingMatrixGT, trackingMatrixErrorCode, numDetectedCells,...
          SNRcontrast, ZvalDistribution, depthDistribution, knnDistribution]...           
           = pairwiseSegmentationLinkageErrorSingleTimePoint(projectName, basenameXMLGMM, imgFilePattern, baseTM)

disp('TODO: count cell division errors')

%fixed parameters
maxDistanceSegmentationError = 10.0; %max distance to consider that two points are the same between ground truth and automatic reconstruction

%main parameters
if(nargin < 1)
    projectName = '12-08-28 Pairwise Random Sampling GT'; %project containing the ground truth in CATMAID
    basenameXMLGMM = 'E:\TGMMruns\GMEMtracking3D_2013_6_6_21_28_43_dataset12-08-28_TM0-1000_NBDan_simpleLogRules_noSL\XML_finalResult_lht\GMEMfinalResult_frame';    
    %basenameXMLGMM = 'E:\TGMMruns\GMEMtracking3D_2013_10_8_20_24_34_afterAddingHSsplitForDeathExtension_TM0-800\XML_finalResult_lht\GMEMfinalResult_frame';
    %basenameXMLGMM = 'E:\TGMMruns\GMEMtracking3D_2013_10_7_17_10_14_afterAdding3waySplit\XML_finalResult_lht\GMEMfinalResult_frame';
    baseTM = 180;
end


%----------------------------------------------------------------------
pathstr = fileparts( mfilename('fullpath') );
pathAdd = [pathstr '/../dbConnection'];
addpath(pathAdd)
%obtain annotated ground truth from CATMAID database for a specific project
%retrieve information to connect to the database
[username, password, databasename, databaseURL] = readDatabaseInfo();

%connect to database
connDB = connectToDatabase(databasename, databaseURL, username, password);
[ projectId, stackId, stackSize, stackRes ] = retrieveProjectInformation( connDB, projectName);

scale = stackRes /stackRes(1);

if( isempty( strfind(projectName,'12-08-28') ) == false || isempty( strfind(projectName,'Drosophila SiMView') ) == false )
    %stack resolution for project should be [406.3, 406.3, 2031]nm = 1000 * [6.5/16, 6.5/16, 2031] = 1000 * [pixelSize / magnification,..., z-step]
    disp 'CORRECTING SCALING FOR PROJECT SINCE CATMAID DOES NOT HAVE CORRECT INFO'
    scale = [1 1  2.031 / (6.5 / 16)];
end

%retrieve all points with confidence 5 (ground truth)
%id, type, x, y, z, radius, parent_id, time, confidence, skeleton_id
WHEREclause = ['project_id = ' '''' num2str(projectId) '''' ' AND confidence=5 AND location_t >=' num2str(baseTM) ' AND location_t<=' num2str(baseTM+1)];
trackingMatrixGT = retrieveLineageFilter( connDB, WHEREclause );

if( isempty( trackingMatrixGT ) == true )
   timeT = [];
   numSegmentationTotal  = [];
   numSegmentationCorrect = []; 
   numLinkageTotal = []; 
   numLinkageCorrect =  [];
   trackingMatrixGT = [];
   trackingMatrixErrorCode = [];
   numDetectedCells = 0;
    SNRcontrast = [];
    ZvalDistribution = [];
    depthDistribution = [];
    knnDistribution = [];
   disp 'No GT points for this time point!!!'
   return;
end

for kk = 1: 3
    trackingMatrixGT(:,kk+2) = scale(kk) * trackingMatrixGT(:,kk + 2) / stackRes(kk);
end

treenodeIdMap = containers.Map(trackingMatrixGT(:,1), [1:size(trackingMatrixGT,1)]);

rmpath( pathAdd );

close(connDB);
%----------------------------------------------------------------------
%%
%start processing each timepoint
addpath 'C:\Users\Fernando\TrackingNuclei\matlabCode\mixtureGaussiansTracking'

minT = min( trackingMatrixGT( : , 8 ) );
maxT = max( trackingMatrixGT( : , 8 ) );

timeT = [minT:maxT]';
numSegmentationTotal = zeros(maxT - minT + 1, 1);
numSegmentationCorrect = numSegmentationTotal;
numLinkageTotal = numSegmentationTotal;
numLinkageCorrect = numSegmentationTotal;
trackingMatrixErrorCode = zeros( size(trackingMatrixGT,1), 1);


distErr = -ones( size(trackingMatrixGT,1), 2);
typeErr = -ones( size(trackingMatrixGT,1), 1);%-1->indicates no error
markedErr = -ones( size(trackingMatrixGT,1), 1);
SNRcontrast = -ones( size(trackingMatrixGT,1), 1);
ZvalDistribution = trackingMatrixGT(:,5) / scale(3);
depthDistribution = -ones( size(trackingMatrixGT,1), 1);
knnDistribution = -ones( size(trackingMatrixGT,1), 1);


numTotalTGMMpts = zeros( maxT+1,1);
numTotalCheckedpts = numTotalTGMMpts;%to calculate the percentage of the data we need to check
numTotalConfidence0pts = numTotalTGMMpts;%at the end, these two variables should agree

%for debugging purposes to analyze errors based on change sin the algorithm
numSegmentationCorrectScore = cell(maxT - minT + 1, 1);
numSegmentationWrongScore = numSegmentationCorrectScore;

tic;


%%
%disp('WARNING: testing short par loop!!!!!!');
for frame = minT : maxT
    
    
    TGMMfilename = [basenameXMLGMM num2str(frame,'%.4d') '.xml'];    
    if( exist(TGMMfilename,'file') == 0 )%we do not have more tracking information
        break
    end
    obj = readXMLmixtureGaussians(TGMMfilename );
    
    if( frame == minT )
        numDetectedCells = length(obj);
    end
    %obj = objCell{frame+1};
        
    GT = trackingMatrixGT(trackingMatrixGT(:,8) == frame, : );
    numSegmentationTotal(frame - minT + 1) = size(GT,1);
    
    trackingMatrixErrorCodeAux = zeros(size(GT,1),1);
   
    
    %load image
    imgFilename = recoverFilenameFromPattern(imgFilePattern, frame);
    [pathstr, name, ext] = fileparts(imgFilename) ;
    
    if( strcmp(ext,'.jp2') == 1 )
        im = readJPEG2000stack(imgFilename, 8 );
    else
        im = readTIFFstack(imgFilename );
    end
    %get interpolated Z between stacks (manual clicking can be off by +-2 slices
    [ZinterpPixels, ~ ] = interpolateZ(im,GT(:,3:5), scale, 2);
    GT(:,5) = ZinterpPixels * scale(3);
    trackingMatrixGT(trackingMatrixGT(:,8) == frame, 5 ) = GT(:,5);%for linkage error
       
    
    %parse xyz points from automatic tracking
    xyz = zeros(length(obj),3);
    parId = zeros(length(obj),1);
    score = zeros(length(obj),1);
    for kk = 1: size(xyz,1)
        xyz(kk,:) = obj(kk).m([2 1 3]);%CATMAID and TGMM flip X Y
        parId(kk) = obj(kk).parent + 1;%matlab index
        score(kk) = obj(kk).splitScore;
        if( obj(kk).id ~= kk-1 )
            error 'GMM Index is not sequential!'
        end
    end                
    for kk = 1:3
        xyz(:,kk) = xyz(:,kk) * scale(kk);
    end
            
    
    %calculate SNRcontrast of annotations
    SNRcontrastAux = sampleDistributionSNR(bsxfun(@mtimes,GT(:,[4 3 5]),1./scale),im);
    %calculate nuclei depth
    depthNucleiAux = sampleDistributionDepth(GT(:,3:5),xyz);
    %calculate nearest neighbors distribution
    knnDistributionAux = sampleDistributionKNN(GT(:,3:5),xyz);
    
    for aa = 1:length(SNRcontrastAux)       
       SNRcontrast( treenodeIdMap( GT(aa,1))) = SNRcontrastAux(aa);
       depthDistribution( treenodeIdMap( GT(aa,1))) = depthNucleiAux(aa);
       knnDistribution( treenodeIdMap( GT(aa,1))) = knnDistributionAux(aa);
    end
    
    numTotalTGMMpts(frame+1) = length(obj);
    numTotalConfidence0pts(frame+1) = sum( score == 0 );
    
    %count segmentation errors
    [idx, dist] = knnsearch(xyz, GT(:,3:5),'k',2);%dist has the size of GT
    [idxErrType, distErrType] = knnsearch(xyz, GT(:,3:5),'k',5);%to calculate types of errors
    pos = find( dist(:,1) < maxDistanceSegmentationError & (dist(:,2)./ dist(:,1)) > 1.5 );
    %pos = find( (dist(:,2)./ dist(:,1)) > 1.5 );
    
    
    numSegmentationCorrect(frame-minT + 1) = length(pos);%second possible match is far away
    trackingMatrixErrorCodeAux( pos ) = 1;
    if( frame == minT )
        trackingMatrixErrorCodeAux( pos ) = 2;%linkage is automatically OK as well
    end
    %TODO: IMPLEMENT MAHALANBIS DISTANCE INSTEAD OF EUCLIDEAN (STILL USING KNN TO PRUNE CANDIDATES);
    %NOTE: WE WOULD NEED precision matrix for GT, since using precision
    %matrix from TGMM does guarantee that the distance is correct since GT
    %is not exhaustive (one big blob could seem like correct segmentation)
    
    
    
    %save info on errors
    posN = [1:size(idx,1)];
    posN(pos) = [];
    for aa = 1:length(posN)
       distErr( treenodeIdMap( GT(posN(aa),1)),: ) = dist(posN(aa),:); 
    end
    
    typeErrAux = zeros(length(posN),1);%0->unknown; 1->cell death; 2->cell division; 3->large displacement
    markedErrAux = typeErrAux;        
    
    numSegmentationCorrectScore{frame-minT + 1} = score( idx(pos,1) );
    numSegmentationWrongScore{frame-minT + 1} = score( idx(posN,1) );
    
    %check if errors havea nearby cell division or death or large
    %displacements
    if( frame > minT )
        %calculate displacement distribution to find out large
        %displacements
        GTold = trackingMatrixGT(trackingMatrixGT(:,8) == frame-1, : );
        [~,dispD] = knnsearch(GT(:,3:5), GTold(:,3:5),'k',1);
        thrD = median(dispD) + 3 *  mad(dispD) * 1.253 ;
        [idxErrTypeDeath, distErrTypeDeath] = knnsearch(xyzOld, GT(:,3:5),'k',5);%to calculate errors based on death
        
        %calculate global stats on TGMM on how many time points we would have to check
        %cell death
        auxParId = parId(parId>0);
        aux = size(xyzOld,1) - length(unique(auxParId));
        numTotalCheckedpts( frame ) = numTotalCheckedpts( frame ) + aux;
        %cell division        
        aux = length(auxParId) - length(unique(auxParId));
        numTotalCheckedpts( frame+1 ) = numTotalCheckedpts( frame+1 ) + aux;
        %large displacement                
        aux = sqrt(sum( ((xyz( parId > 0, : ) - xyzOld( auxParId , :)) ).^2,2));
        numTotalCheckedpts( frame+1 ) = numTotalCheckedpts( frame+1 ) + sum(aux > thrD);
        
        for kk = 1:length(posN)
            
            pp = posN(kk);
            %check if neighbors (in time and space) have a division
            %TODO: expand time search to +-2 time points (right now only to
            %-1)
            
            %check if neighbors in previous time point died            
            for aa = 1:size(idxErrTypeDeath,2)
                auxParId = idxErrTypeDeath( pp, aa);%this is ID in xyzOld
                if( sum( parId == auxParId ) == 0 )
                    typeErrAux(kk) = 1;%cell death
                end
                if( scoreOld( auxParId ) == 0 )
                    markedErrAux(kk) = 1;%it was marked in CATMAID
                end
            end                        
            
            if( typeErrAux(kk) == 0 )%not assigned yet
                for aa = 1:size(idxErrType,2)
                    
                    auxParId = idxErrType( pp, aa);
                    if( score( auxParId ) == 0 )
                        markedErrAux(kk) = 1;%it was marked in CATMAID
                    end
                    
                    if (sum( parId == parId( auxParId ) ) > 1 )
                        typeErrAux(kk) = 2;%cell division
                        break;
                    end
                    %check if it is large displacement
                    if( parId( auxParId ) > 0 )
                        if( norm (xyz( auxParId ,: ) - xyzOld( parId( auxParId ), :)) > thrD )
                            typeErrAux(kk) = 3;%large displacement
                        end
                    end
                    
                    %previous time point
                    auxParId = parId( idxErrType( pp, aa) );
                    if( scoreOld( auxParId) == 0 )
                        markedErrAux(kk) = 1;
                    end
                    if (sum( parIdOld == parIdOld( auxParId ) ) > 1 )
                        typeErrAux(kk) = 2;%cell division
                        break;
                    end
                    
                end
            end                                                
        end
    end
    
    for aa = 1:length(posN)
       typeErr( treenodeIdMap( GT(posN(aa),1))) = typeErrAux(aa); 
       markedErr( treenodeIdMap( GT(posN(aa),1))) = markedErrAux(aa);
    end
    
    %count linkage errors
    if( frame > minT )
        numLinkageTotal(frame-minT + 1) = length(pos);
        for kk = 1:length(pos)
            nodeId = GT(pos(kk),1);
            nodeIdPar = trackingMatrixGT( treenodeIdMap(nodeId), 7 );
            if( isKey( treenodeIdMap, nodeIdPar ) == false )%we do not have parent in ground truth
                numLinkageTotal(frame-minT + 1) = numLinkageTotal(frame-minT + 1) - 1;
                continue;
            end
            xyzParGT = trackingMatrixGT( treenodeIdMap(nodeIdPar), 3:5);
            
            %find xyz for parent in automatic tracking
            parIdM = parId( idx(pos(kk),1) );
            if( parIdM <= 0 )
                continue;%mistake
            end
            xyzPar = xyzOld( parIdM, :);
            
            %only count this if parIdM was part of the correct segmentation
            if( sum(parIdM == idxOld( posOld,1 ) ) == 0 )
                numLinkageTotal(frame-minT + 1) = numLinkageTotal(frame-minT + 1) - 1;
                continue;
            end
            
            if( norm([xyzPar-xyzParGT] ) < maxDistanceSegmentationError )
                numLinkageCorrect(frame-minT+1) = numLinkageCorrect(frame-minT+1) + 1;
                trackingMatrixErrorCodeAux( pos(kk) ) = 2;   
            else
                a = 1;%for debugging
            end            
            
            
        end
    end
    
    
    trackingMatrixErrorCode(trackingMatrixGT(:,8) == frame ) = trackingMatrixErrorCodeAux;
    
    idxOld = idx;
    %idxErrTypeOld = idxErrType;
    %distErrTypeOld = distErrType;
    posOld = pos;
    %distOld = dist;
    xyzOld = xyz;
    parIdOld = parId;
    scoreOld = score;
    %GTold = GT;
    %imOld = im;
    %objOld = obj;        
end %end for frame:minT:maxT
rmpath 'C:\Users\Fernando\TrackingNuclei\matlabCode\mixtureGaussiansTracking'


%%
display(['Total of ' num2str(sum(numSegmentationTotal)) ' nodes in GT. Segmentation accuracy is ' num2str( sum(numSegmentationCorrect) / sum(numSegmentationTotal) ) '.Linkage accuracy is ' num2str( sum(numLinkageCorrect) / sum(numLinkageTotal) )]);



