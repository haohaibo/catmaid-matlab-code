%INPUT:
%labelSkeleton: use only points that belong to skeletons with a certain
%label. Set as empty to ignore this filter.
%we just look at solution at TM and TM+1 (basically pairwise random annotations)
function [ trackingMatrixGT, centroidDistance, centroidResidual, linkageError, SNRcontrast, ZvalDistribution, depthDistribution, knnDistribution, knnBase, numPtsXYZ] = ... 
            pairwiseSoftSegmentationLinkageErrorSingleTimePoint(projectName, basenameXMLGMM, imgFilePattern, baseTM, labelSkeleton, flipCoord)


%main parameters
if(nargin < 1)
    projectName = '12-08-28 Pairwise Random Sampling GT'; %project containing the ground truth in CATMAID
    %basenameXMLGMM = 'E:\TGMMruns\GMEMtracking3D_2013_6_6_21_28_43_dataset12-08-28_TM0-1000_NBDan_simpleLogRules_noSL\XML_finalResult_lht\GMEMfinalResult_frame';    
    basenameXMLGMM = 'E:\TGMMruns\GMEMtracking3D_2013_10_8_20_24_34_afterAddingHSsplitForDeathExtension_TM0-800\XML_finalResult_lht\GMEMfinalResult_frame';
    %basenameXMLGMM = 'E:\TGMMruns\GMEMtracking3D_2013_10_7_17_10_14_afterAdding3waySplit\XML_finalResult_lht\GMEMfinalResult_frame';
    baseTM = 130;
    imgFilePattern = 'G:/12-08-28/Results/TimeFused.Blending/Dme_E1_His2ARFP.TM?????_timeFused_blending/SPC0_CM0_CM1_CHN00_CHN01.fusedStack_?????.jp2';
    flipCoord = false;
end


%----------------------------------------------------------------------
pathstr = fileparts( mfilename('fullpath') );
pathAdd = [pathstr '/../dbConnection'];
addpath(pathAdd)
%obtain annotated ground truth from CATMAID database for a specific project
%retrieve information to connect to the database
[username, password, databasename, databaseURL] = readDatabaseInfo();

%connect to database
connDB = connectToDatabase(databasename, databaseURL, username, password);
[ projectId, stackId, stackSize, stackRes ] = retrieveProjectInformation( connDB, projectName);

scale = stackRes /stackRes(1);

if( isempty( strfind(projectName,'12-08-28') ) == false || isempty( strfind(projectName,'Drosophila SiMView') ) == false )
    %stack resolution for project should be [406.3, 406.3, 2031]nm = 1000 * [6.5/16, 6.5/16, 2031] = 1000 * [pixelSize / magnification,..., z-step]
    disp 'CORRECTING SCALING FOR PROJECT SINCE CATMAID DOES NOT HAVE CORRECT INFO'
    scale = [1 1  2.031 / (6.5 / 16)];
end

%retrieve all points with confidence 5 (ground truth)
%id, type, x, y, z, radius, parent_id, time, confidence, skeleton_id
WHEREclause = ['project_id = ' '''' num2str(projectId) '''' ' AND confidence=5 AND location_t >=' num2str(baseTM) ' AND location_t<=' num2str(baseTM+1)];
trackingMatrixGT = retrieveLineageFilter( connDB, WHEREclause );


% % % % uncomment for mouse dataset (because we cropped it in time)
% % % disp '=========WARNING:USING SPECIAL TIME SHIFT FOR MOUSE===================='
% % % WHEREclause = ['project_id = ' '''' num2str(projectId) '''' ' AND confidence=5 AND location_t >=' num2str(baseTM-150) ' AND location_t<=' num2str(baseTM+1-150)];
% % % trackingMatrixGT = retrieveLineageFilter( connDB, WHEREclause );
% % % trackingMatrixGT(:,8) = trackingMatrixGT(:,8) + 150;

if( isempty( trackingMatrixGT ) == true )   
    SNRcontrast = [];
    ZvalDistribution = [];
    depthDistribution = [];
    knnDistribution = [];
    trackingMatrixGT = [];
    centroidDistance = [];
    centroidResidual = [];
    linkageError = [];
    knnBase = 1.0;
    numPtsXYZ = [];
   disp 'No GT points for this time point!!!'
   return;
end

for kk = 1: 3
    trackingMatrixGT(:,kk+2) = scale(kk) * trackingMatrixGT(:,kk + 2) / stackRes(kk);
end

treenodeIdMap = containers.Map(trackingMatrixGT(:,1), [1:size(trackingMatrixGT,1)]);

rmpath( pathAdd );


%Database connections, cursors, and resultset objects remain open until you close them using the close function. 
%Always close a cursor, connection, or resultset when you finish using it. Close a cursor before closing the connection used for that cursor.
close(connDB);

%%
%special case for mouse after applying drift correction
%%apply translation to points to match drifted image
%parameters from drift correction:WE ASSUME ROI starts at 1 (so no
%cropping)
%{
disp '=================SPECIAL DRIFT CORRECTION APPLIED TO MOUSE!!!!================='
masterTimepoint = 100;
timepoints     = 0:186;
parameterPath = 'X:\SiMView1\13-09-08\Drift\'
load([parameterPath '\lookUpTable.mat']);

%reproduce offsets
nTimepoints = length(timepoints);
xOffsets = interp1(lookUpTable(:, 1), lookUpTable(:, 2), timepoints);
yOffsets = interp1(lookUpTable(:, 1), lookUpTable(:, 3), timepoints);
zOffsets = interp1(lookUpTable(:, 1), lookUpTable(:, 4), timepoints);
correctStep = (zOffsets(end) - zOffsets(1))/(nTimepoints-1);
zOffsets = zOffsets - correctStep * (0:nTimepoints-1);

masterTPIndex = find(timepoints==masterTimepoint);
xOffsets = -xOffsets + xOffsets(masterTPIndex);
yOffsets = -yOffsets + yOffsets(masterTPIndex);
zOffsets = -zOffsets + zOffsets(masterTPIndex);


xOffsets = xOffsets * scale(1);
yOffsets = yOffsets * scale(2);
zOffsets = zOffsets * scale(3);

TPindex = find(timepoints == baseTM );

T = -[yOffsets(TPindex) xOffsets(TPindex) zOffsets(TPindex)];

for kk = 1: 3
    trackingMatrixGT(:,kk+2) = trackingMatrixGT(:,kk + 2) + T(kk);
end
%}

%%
%calculate baseKNN distance
addpath 'C:\Users\Fernando\TrackingNuclei\matlabCode\mixtureGaussiansTracking'
baseTMref = 20;%time point to use as a reference for nearest neighbor
TGMMfilename = [basenameXMLGMM num2str(baseTMref,'%.4d') '.xml'];
while( exist(TGMMfilename,'file') == 0 && baseTMref < 1000 )
    baseTMref = baseTMref + 10;%time point to use as a reference for nearest neighbor
    TGMMfilename = [basenameXMLGMM num2str(baseTMref,'%.4d') '.xml'];
end
if( exist(TGMMfilename,'file') == 0 )%we do not have more tracking information
    error 'Time point for reference frame does not exist'
end

if( isempty(strfind(basenameXMLGMM,'MacOSX')) == false )
    %it does not have svIdx 
    obj = readXMLmixtureGaussiansM(TGMMfilename );
    for ii = 1:length(obj)
        obj(ii).m = obj(ii).m([2 1 3]);%we used tiff here
        obj(ii).svIdx = [];
    end
else%regular case
    obj = readXMLmixtureGaussians(TGMMfilename );
end

%parse xyz points from automatic tracking
xyz = zeros(length(obj),3);
parId = zeros(length(obj),1);
for kk = 1: size(xyz,1)
    xyz(kk,:) = obj(kk).m([2 1 3]);%CATMAID and TGMM flip X Y
    parId(kk) = obj(kk).parent + 1;%matlab index
    if( obj(kk).id ~= kk-1 )
        error 'GMM Index is not sequential!'
    end
end
for kk = 1:3
    xyz(:,kk) = xyz(:,kk) * scale(kk);
end
[~,dist] = knnsearch(xyz,xyz,'k',2);
knnBase = mean(dist(:,2));

%----------------------------------------------------------------------
%%
%start processing each timepoint
addpath 'C:\Users\Fernando\TrackingNuclei\matlabCode'

minT = min( trackingMatrixGT( : , 8 ) );
maxT = max( trackingMatrixGT( : , 8 ) );


SNRcontrast = -ones( size(trackingMatrixGT,1), 1);
ZvalDistribution = trackingMatrixGT(:,5) / scale(3);
depthDistribution = -ones( size(trackingMatrixGT,1), 1);
knnDistribution = -ones( size(trackingMatrixGT,1), 1);

centroidDistance = -ones( size(trackingMatrixGT,1), 3);%three tupes of distance: raw, Mahalanobis, normalized by knn stats
centroidResidual = -ones( size(trackingMatrixGT,1), 3);%in case we want to calculate uncertainty as a covariance matrix
linkageError = -ones( size(trackingMatrixGT,1), 1);


%%
%disp('WARNING: testing short par loop!!!!!!');
for frame = minT : maxT
    
    
    TGMMfilename = [basenameXMLGMM num2str(frame,'%.4d') '.xml'];    
    if( exist(TGMMfilename,'file') == 0 )%we do not have more tracking information
        break
    end
    
    
    if( isempty(strfind(basenameXMLGMM,'MacOSX')) == false )
        %it does not have svIdx
        obj = readXMLmixtureGaussiansM(TGMMfilename );
        for ii = 1:length(obj)
            obj(ii).m = obj(ii).m([2 1 3]);%we used tiff here
            obj(ii).svIdx = [];
        end
    else%regular case
        obj = readXMLmixtureGaussians(TGMMfilename );
    end
    
    GT = trackingMatrixGT(trackingMatrixGT(:,8) == frame, : );           
   
    
    %load image
    imgFilename = recoverFilenameFromPattern(imgFilePattern, frame);
    [pathstr, name, ext] = fileparts(imgFilename) ;
    
    if( strcmp(ext,'.jp2') == 1 )
        im = readJPEG2000stack(imgFilename, 8 );
    else
        im = readTIFFstack(imgFilename );
    end
    
    %read list of supervoxels
    svFilename = [basenameXMLGMM num2str(frame,'%.4d') '.svb'];
    if( exist(svFilename,'file') == 0 )%we do not have more tracking information
        svList = [];
        sizeIm = size(im);
    else
        [svList, sizeIm] = readListSupervoxelsFromBinaryFile( svFilename );
    end

    
    
    if( isempty(sizeIm) )%we do not have supervoxel file ( some programs do not output that )
       sizeIm = size(im);
       useSv = false;
    else
        useSv = true;
    end
           
    
    %get interpolated Z between stacks (manual clicking can be off by +-2 slices
    [ZinterpPixels, ~ ] = interpolateZ(im,GT(:,3:5), scale, 2);
    GT(:,5) = ZinterpPixels * scale(3);
    trackingMatrixGT(trackingMatrixGT(:,8) == frame, 5 ) = GT(:,5);%for linkage error
       
    
    %parse xyz points from automatic tracking
    xyz = zeros(length(obj),3);
    parId = zeros(length(obj),1);
    for kk = 1: size(xyz,1)
        xyz(kk,:) = obj(kk).m([2 1 3]);%CATMAID and TGMM flip X Y
        parId(kk) = obj(kk).parent + 1;%matlab index
        if( obj(kk).id ~= kk-1 )
            error 'GMM Index is not sequential!'
        end
    end                
    for kk = 1:3
        xyz(:,kk) = xyz(:,kk) * scale(kk);
    end
    
     if( ( size(im,1) == sizeIm(2) && size(im,2) == sizeIm(1) && size(im,1) ~= size(im,1) && size(im,3) == sizeIm(3) ) || flipCoord == true)%flip coordinates
        disp '============FLIPPING COORDINATES!!!!===================' 
        sizeIm = size(im);
        xyz = xyz(:,[2 1 3]);
    end
    
    %update knn base
    [~,XYZknnDist] = knnsearch(xyz,xyz,'k',2);
    XYZknnDist = XYZknnDist(:,2);
    if( frame == minT )
        knnBase = mean(XYZknnDist) / knnBase;%correction factor for different cell density at different developmental states
        numPtsXYZ = size(xyz,1);
    end
    
    %calculate SNRcontrast of annotations
    SNRcontrastAux = sampleDistributionSNR(bsxfun(@mtimes,GT(:,[4 3 5]),1./scale),im);
    %calculate nuclei depth
    ppAux = find( xyz(:,1) > 0 );%to avoid -1e32 elements
    depthNucleiAux = sampleDistributionDepth(GT(:,3:5),xyz(ppAux,:));
    %calculate nearest neighbors distribution
    knnDistributionAux = sampleDistributionKNN(GT(:,3:5),xyz,1);%we only consider one-neighbor for the normalization (to detect oversegmentation)
    
    for aa = 1:length(SNRcontrastAux)       
       SNRcontrast( treenodeIdMap( GT(aa,1))) = SNRcontrastAux(aa);
       depthDistribution( treenodeIdMap( GT(aa,1))) = depthNucleiAux(aa);
       knnDistribution( treenodeIdMap( GT(aa,1))) = knnDistributionAux(aa);
    end
    
    
    %find which GMM contains each GT point
    [idx, ~] = knnsearch(xyz,GT(:,3:5),'k',10);%dist has the size of GT. To restrict the search, we bet that the assignment is within the 10-nearest neighbros (usually is the nearest neighbor but not always)
    
    if( useSv == false )
        %use knn directly
        matchGT2TGMM = idx(:,1);
    else%find which supervoxel contain GT point
        
        matchGT2TGMM = zeros(size(idx,1),1);
        for kk = 1:size(idx,1)
            auxIdx = sub2ind(sizeIm,round(1 + GT(kk,4)/scale(2)), round(1 + GT(kk,3)/scale(1)), round( 1 + GT(kk,5)/scale(3))) - 1;
            for aa = 1:size(idx,2)
                svIdxVec = obj( idx(kk,aa) ).svIdx;
                for bb = 1:length(svIdxVec)
                    if( svIdxVec(bb) < 0 )
                        continue;
                    end
                    aux = svList{svIdxVec(bb) + 1};
                    if(  min(abs((aux - auxIdx))) < 7 ) %found the match
                        matchGT2TGMM(kk) = idx(kk,aa);
                        break;
                    end
                end
                if( matchGT2TGMM(kk) > 0 )
                    %recompute precision matrix (I might not have that info from
                    %[sx, sy, sz] = ind2sub(imSize, PixelIdxList+1); TODO if
                    %necessary
                    break;
                end
            end
            if( matchGT2TGMM(kk) == 0 )
                %we just assign it to the nearest neighbor
                %TODO: check why in a few case this happens
                matchGT2TGMM(kk) = idx(kk, 1);
            end
        end
    end
    
    %update distance metrics
    for aa = 1 : size(GT,1)
       auxDist1 = norm(GT(aa,3:5) - xyz(matchGT2TGMM(aa),:));%raw distance
       %auxDist3 = auxDist1 / knnDistributionAux(aa);%normalized by knn stats
       auxDist3 = auxDist1 / XYZknnDist(matchGT2TGMM(aa));
       mu = (GT(aa,3:5) - xyz(matchGT2TGMM(aa),:) )./scale;
       auxDist2 =  mu * obj(matchGT2TGMM(aa)).W * obj(matchGT2TGMM(aa)).nu *mu';%mahalanobis distance
       
       centroidDistance( treenodeIdMap( GT(aa,1)),: ) = [auxDist1 auxDist2 auxDist3];
       centroidResidual( treenodeIdMap( GT(aa,1)),: ) = GT(aa,3:5) - xyz(matchGT2TGMM(aa),:);
    end
    
    %update linkage error
    if( frame > minT )
        GToldIdMap = containers.Map(GTold(:,1), [1:size(GTold,1)]);
        for aa = 1:size(GT,1)
            if( isKey( GToldIdMap, GT(aa,7) ) == true )
               if( matchGT2TGMMold(GToldIdMap(GT(aa,7))) == parId( matchGT2TGMM(aa) ) ) %good linkage
                   linkageError( treenodeIdMap( GT(aa,1)) ) = 1;
               else
                   linkageError( treenodeIdMap( GT(aa,1)) ) = 0;
               end
            end
        end
    end
    
    %update information from previous time point
    GTold = GT;
    matchGT2TGMMold = matchGT2TGMM;        
end %end for frame:minT:maxT


%%
%keep elements with a certain label
if( isempty( labelSkeleton ) == false )
    %find all the skeleton id that have the label
    [~, skeletonId] = retrieveDatabaseIDwithSpecificTag(projectName, labelSkeleton);
    %find intersection of skeletons
    [~,IA,~] = intersect(trackingMatrixGT(:,10),skeletonId);
    trackingMatrixGT = trackingMatrixGT(IA,:);
    centroidDistance = centroidDistance(IA,:);
    centroidResidual = centroidResidual(IA,:);
    linkageError = linkageError(IA);
    SNRcontrast = SNRcontrast(IA);
    ZvalDistribution = ZvalDistribution(IA);
    depthDistribution = depthDistribution(IA);
    knnDistribution = knnDistribution(IA);
    %knnBase = knnBase;
    %numPtsXYZ = numPtsXYZ
end 

%%
rmpath 'C:\Users\Fernando\TrackingNuclei\matlabCode\mixtureGaussiansTracking'
rmpath 'C:\Users\Fernando\TrackingNuclei\matlabCode'





