This file contains basic instructions for running the Matlab scripts created to import and export cell lineaging and imaging data from CATMAID server.

1.-CONTENTS OF THE SOFTWARE ARCHIVE
-----------------------------------------------------------------------------------
We assume that the user has cloned all the files in a folder of their choice, referred to here as $ROOT_CATMAID_MATLAB. This root folder contains the license agreement and this README file. The subfolders in $ROOT_CATMAID_MATLAB contain the following components:

-dbConnection: scripts to communicate with the CATMAID database and extract cell lineaging information using SQL commands.

-generateTiles: scripts to transform time lapse image data into image tiles to store in the image server where CATMAID can load them and display them on a browser a per user request.

-filters: different filters that act in the cell lineaging tracks. For example, deleting branches that are too short after cell division.

-errorAnalysis: scripts used to 


2.-RETRIEVE TRACKING INFORMATION FROM THE DATABASE
-----------------------------------------------------------------------------------

A.-Open the Matlab script $ROOT_CATMAID_MATLAB/dbConnection/scriptRetrieveNodeWithTag.m with any text editor.

B.-Setup the projectName variable to the CATMAID project you want to retrieve information from.

C.-Make sure the file $ROOT_CATMAID_MATLAB/dbConnection/databaseInfo.txt exists and contains the following information (one element per line):

username 
password
databasename
databaseURL

The file $ROOT_CATMAID_MATLAB/dbConnection/databaseInfoExample.txt contains a template of how the file should look like. This file is read by the Matlab script to extract the login information into the CATMAID database.
This method allows to distribute the code without compromising database secutiry between users. We recommend to change the permissions of the file databaseInfo.txt so only the user can read it.

D.-Run the Matlab script. It will return the following variables:

stackRes: 	is 1x3 array indicating the spatial sampling for the dataset (nm per pixel)

stackSize: 	is a 1x3 array indicating the dimensions (in pixels) of each image stack in the time-lapse

trackingM:	is an Nx10 Matlab array, where N is the number of points in the database for the requested project. Each of the columns contains the following information:

1.	Unique Id from the database to identify the point ( a large integer number)
2.	Cell type (represented by an integer). It is 0 if no cell type has been set for this object.
3.	x location of the nucleus centroid in world coordinates. Use the variable stackRes to convert from world coordinates to pixel unites.
4.	Same as 3 but for y location.
5.	Same as 3 but for z location.
6.	Estimated radius of the nucleus. It is 0 if this parameter was not estimated.
7.	Id of the cell in the previous time point. It is -1 if there is no linkage. Otherwise it has the unique id of the parent from column 1, so you can reconstruct the lineage.
8.	Time point of the nucleus.
9.	Confidence level in the tracking result. Value of 3 indicates high confidence that the object was correctly tracked. Value of 0 indicates low confidence.
10.	Skeleton id. All cells belonging to the same lineage have the same unique skeleton id.

tags: 		is an NxM cell array containing the tags for each data point. M is the maximum number of tags corresponding to a single data point.


3.-PARSE IMAGE DATA INTO TILES 
-----------------------------------------------------------------------------------

A.-Open the Matlab script $ROOT_CATMAID_MATLAB/generateTiles/generateTilesFromFolder.m

B.-Adjust the following variables in the initial lines of the script:

numWorkers:			number of workers to carry the work in parallel using the Matlab Parallel Toolbox. It should not exceed the number of cores in the machine. Depending on the I/0 hardware connecting to the image server, you might not want to select more than 6 workers to avoid overloading data.

outputCATMAIDprojectFolder:	folder where tiles are going to be written. If it does not exist, the script will create the last subfolder. This address should be visible by CATMAID server, so it can read the tiles and send them to the user's web browser.

imgFilenamePatternList:		cell array containing the file pattern. Each element of the cell array will be considered as a separate channel. The file pattern nomenclature uses '?' as a place holder for digits following the standards of regular expressions. In other words, any '?' symbol will be substituted by time point number.

iniFrame:			scalar with the initial time point to convert into tiles. Typically this is 0 or 1.

endFrame:			scalar with the last time point to convert into tiles.


tileSize:			size (in pixels) of each tile. Recommended values are powers of 2 such as 512, 1024 or 2048. For example, if tileSize is equal to 1024, the script partitions each XY slice from an image stack into 1024x1024 images (tiles) using zero padding to extend the slice if necessary. Then, CATMAID server will compose a mosaic of each slice in the brwoser by superimposing these tiles.

thumbnailSize:			size (in pixels) of a small image representing the dataset to be placed in the CATMAID Projects website. A typical value of 64 should be OK for all elements.

maxScales:			postive integer indicating the number of scales to be generated in the pyramid. If value is equal to 1, then only the original resolution is saved. For slices smaller than the monitor resolution, 1 should be the default value.

imType:				image type extension to use for the tiles. It should a format compatible with the type of images that browsers can display. '.jpg' or '.png' are the most common settings.

intensityScaling:		integer indicating the kind of image contrast reescaling performed to transform each image stack into an 8-bit image with full dynamic range. Value of 0 indicates a global linear scaling. Value of 1 indicates the use of Contrast Limited Adaptive Histogram Equalization (CLAHE).

maxNucleusRadius		positive integer indicating the maximum radius of an object to estimate CLAHE. This value should be set conservatively (much higher than the radius of the largest cell) to avoid oversaturation in images.

C.-Run the Matlab script. Depending on the size of the dataset it can take a few hours to generate the tiles and transfer all of them to the image server.
