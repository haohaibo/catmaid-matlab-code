%recursive function to generate tiles
function generateTile(slice,dstFolder, tileSize, numScales, maxScales, imType)
%INPUT: slice: NxM array single or double type with values between [0,1]

if( size( slice, 1 ) < tileSize || size( slice, 2 ) < tileSize )
    slice = padarray(slice,max(tileSize - size(slice),0) ,0,'post');
end

if( mod( size( slice, 1 ) , tileSize ) ~= 0  || mod( size( slice, 2) , tileSize ) ~= 0 )
    error 'Slice should be a multiple of tile size'
end

if (numScales >= maxScales )
    return;
end

numTiles = size( slice) / tileSize;

for ii = 0 : numTiles(1)-1
    for jj = 0 : numTiles(2)-1
        tile = slice( tileSize * ii + 1: tileSize * (ii+1), tileSize * jj + 1: tileSize * (jj+1) );
        tileFilename = [dstFolder '/' num2str(ii) '_' num2str(jj) '_' num2str(numScales) imType];
        imwrite( uint8( tile * 255 ), tileFilename );
    end
end

%generate tiles for next scale
slice = imresize(slice, 0.5, 'bicubic','Antialiasing',true);
generateTile(slice,dstFolder, tileSize, numScales + 1, maxScales, imType);

end