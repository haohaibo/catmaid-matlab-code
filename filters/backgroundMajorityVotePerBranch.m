function backgroundMajorityVotePerBranch(trackingM)

N = size(trackingM,1);
nodeIdMap = containers.Map(trackingM(:,1),[1:N]);



%find num children for each node
numCh = zeros(N,1);

for kk = 1: N
    par = trackingM(kk,7);
    
    if( par < 0 || isKey(nodeIdMap, par)== false )
        continue;
    end
    aux = nodeIdMap( par );
    numCh ( aux ) = numCh( aux ) + 1;
end

%find leaves: for each leave
posLeaves = find( numCh == 0 );


