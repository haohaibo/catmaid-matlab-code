%trackingM contains id, type, x, y, z, radius, parent_id, time, confidence, skeleton_id
function [trackingM, erase] = deleteShortLivedBranches(trackingM, thr)

N = size(trackingM,1);
nodeIdMap = containers.Map(trackingM(:,1),[1:N]);



%find num children for each node
numCh = zeros(N,1);

for kk = 1: N
    par = trackingM(kk,7);
    
    if( par < 0 || isKey(nodeIdMap, par)== false )
        continue;
    end
    aux = nodeIdMap( par );
    numCh ( aux ) = numCh( aux ) + 1;
end

%for each leaf node( numCh == 0 ) backtrack to find short lived
pos = find( numCh == 0 );%last frame should not be counted as death

erase = zeros(round(N/10),1);
eraseN = 0;
numF = 0;
for kk = 1: length(pos)
    count = 1;% we already have th eleave
    
    par = trackingM( pos(kk), 7 );
    eraseAux = pos(kk);
    while( count <= thr )
        if( par < 0 || isKey(nodeIdMap, par)== false )
            break;
        end
        
        parPos = nodeIdMap( par );
        if( numCh( parPos ) == 2 )% found a division
            break;
        end
        
        %update elements
        eraseAux = [eraseAux parPos];
        count = count + 1;
        par = trackingM( parPos, 7 );
    end
    
    if( count <=thr )%we exit earlier
       erase(eraseN+1 : eraseN + length(eraseAux)) = eraseAux;
       eraseN = eraseN + length(eraseAux);
       numF = numF + 1;
    end
end

erase(eraseN+1:end) = [];

%delete filtered elements
trackingM(erase,:) = [];

disp(['Erased ' num2str(numF) ' short lived branches out of ' num2str(length(pos)) ' leave nodes']);

